<?php

//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'sk_enqueue_scripts' );
function sk_enqueue_scripts() {
 
	wp_enqueue_script( 'equalheights', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.equalheights.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'equalheights-init',  get_stylesheet_directory_uri() . '/js/equalheights-init.js', array( 'equalheights' ), '1.0.0', true );
 
}

?>
