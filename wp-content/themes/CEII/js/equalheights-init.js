jQuery(document).ready(function($) {
 
	$(window).load(function() {
		if (window.innerWidth > 2000) {
			$('.announcements .et_pb_column_1_2').equalHeights();
		}
	});
 
	$(window).resize(function(){
		if (window.innerWidth > 2000) {
			$('.announcements .et_pb_column_1_2').height('auto');
			$('.announcements .et_pb_column_1_2').equalHeights();
		}
	});
 
});